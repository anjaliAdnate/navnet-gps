import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Navbar, Events, Platform, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, LatLngBounds, Geocoder, GeocoderResult, GoogleMapsMapTypeId, ILatLng } from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { ModalPage } from './modal';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-history-device',
  templateUrl: 'history-device.html',
})
export class HistoryDevicePage implements OnInit, OnDestroy {
  @ViewChild(Navbar) navBar: Navbar;


  shouldBounce = true;
  dockedHeight = 100;
  distanceTop = 378;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 0;

  showActionSheet: boolean = false;
  transition: any = ['0.5s', 'ease-in-out'];

  device: any;
  trackerId: any;
  trackerType: any;
  DeviceId: any;
  datetimeStart: any;
  datetimeEnd: any;
  hideplayback: boolean;
  trackerName: any;
  avg_speed: string;
  total_dis: string;
  data2: any;
  latlongObjArr: any;
  locations: any = [];
  islogin: any;
  dataArrayCoords: any = [];
  mapData: any[];
  speed: number;
  flightPath: any;
  arrival: Date;
  departure: Date;
  target: number;
  playing: boolean;
  coordreplaydata: any;
  speedMarker: any;
  updatetimedate: any;
  showDropDown: boolean;
  SelectVehicle: string = 'Select Vehicle';
  // devices1243: any[];
  // devices: any;
  // isdevice: any;
  portstemp: any;
  selectedVehicle: any;
  totime: string;
  fromtime: string;
  allData: any = {};
  startPos: any[];
  showZoom: boolean = false;
  address: any;
  arrTime: any;
  depTime: any;
  addressofstudent: any;
  drawerHidden1: boolean;
  arrivalTime: string;
  departureTime: string;
  addressof: string;
  durations: string;
  menuActive: boolean;
  mapKey: string;
  idleLocations: any[];
  addressof123: string;
  latLngArray: any = [];
  devices: any = [];
  markersArray: any = [];
  fraction: number = 0;
  intevalId: any;
  zoomLevel: number = 15;
  direction: number = 1;
  vehicle_speed: number;
  cumu_distance: any;
  recenterMeLat: any;
  recenterMeLng: any;
  addressLine: string;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private plt: Platform,
    private translate: TranslateService,
    private modalCtrl: ModalController,
    private geocoderApi: GeocoderProvider
  ) {
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == this.translate.instant('Hybrid')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == this.translate.instant('Normal')) {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == this.translate.instant('Terrain')) {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == this.translate.instant('Satellite')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }
  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        // todo something
        // this.navController.pop();
        console.log("back button poped")
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage')
            }
          }
        }
      }
    }

    localStorage.removeItem("markerTarget");
    // localStorage.removeItem("speedMarker");
    // localStorage.removeItem("updatetimedate");

    if (localStorage.getItem("MainHistory") != null) {
      console.log("coming soon")
      this.showDropDown = true;
      this.getdevices();
    } else {
      this.device = this.navParams.get('device');
      console.log("devices=> ", this.device);
      this.trackerId = this.device.Device_ID;
      this.trackerType = this.device.iconType;
      this.DeviceId = this.device._id;
      this.trackerName = this.device.Device_Name;
      this.btnClicked(this.datetimeStart, this.datetimeEnd)
    }
    this.hideplayback = false;
    this.target = 0;
  }


  ngOnDestroy() {
    localStorage.removeItem("markerTarget");
    // localStorage.removeItem("speedMarker");
    // localStorage.removeItem("updatetimedate");
    localStorage.removeItem("MainHistory");
    if (this.intevalId) {
      clearInterval(this.intevalId);
    }
  }

  changeformat(date) {
    console.log("date=> " + new Date(date).toISOString())
  }
  setDocHeight() {
    console.log("dockerchage event")
    this.dockedHeight = 150;
    this.distanceTop = 46;
  }

  closeDocker() {
    let that = this;
    that.showActionSheet = false;
  }
  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
        // this.devices1243 = [];
        // this.devices = data;
        // this.devices1243.push(data);
        // localStorage.setItem('devices', this.devices);
        // this.isdevice = localStorage.getItem('devices');
        // for (var i = 0; i < this.devices1243[i]; i++) {
        //   this.devices1243[i] = {
        //     'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
        //   };
        // }
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }

  onChangedSelect(item) {
    let that = this;
    that.trackerId = item.Device_ID;
    that.trackerType = item.iconType;
    that.DeviceId = item._id;
    that.trackerName = item.Device_Name;
    if (that.allData.map) {
      that.allData.map.clear();
      that.allData.map.remove();
    }
  }

  reCenterMe() {
    // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      zoom: this.allData.map.getCameraZoom()
    }).then(() => {

    })
  }

  Playback() {
    let that = this;
    that.showZoom = true;
    if (localStorage.getItem("markerTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("markerTarget"));
    }
    that.playing = !that.playing; // This would alternate the state each time

    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    that.startPos = [lat, lng];
    that.speed = 200; // km/h

    if (that.playing) {
      that.allData.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.allData.mark == undefined) {
        var icicon;
        if (that.plt.is('ios')) {
          icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
        } else if (that.plt.is('android')) {
          icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
        }
        that.allData.map.addMarker({
          icon: icicon,
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(that.startPos[0], that.startPos[1]),
        }).then((marker: Marker) => {
          that.allData.mark = marker;
          that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
        });
      } else {
        // that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
      }
    } else {
      that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
    }
  }

  getIconUrl() {
    let that = this;
    var iconUrl;
    if (that.plt.is('ios')) {
      iconUrl = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
    } else if (that.plt.is('android')) {
      iconUrl = './assets/imgs/vehicles/running' + that.trackerType + '.png';
    }
    console.log("icon url: ", iconUrl);
    return iconUrl;
  }

  // Playback() {
  //   let that = this;
  //   that.showZoom = true;
  //   if (localStorage.getItem("markerTarget") != null) {
  //     that.target = JSON.parse(localStorage.getItem("markerTarget"));
  //   }
  //   that.playing = !that.playing; // This would alternate the state each time

  //   var coord = that.dataArrayCoords[that.target];
  //   that.coordreplaydata = coord;
  //   var lat = coord[0];
  //   var lng = coord[1];

  //   that.startPos = [lat, lng];
  //   that.speed = 200; // km/h
  //   // debugger
  //   if (that.playing) {
  //     var d4 = that.dataArrayCoords;
  //     console.log(JSON.parse(JSON.stringify(d4)));
  //     if (d4 != undefined) {
  //       if (d4.length > 0) {
  //         that.innerFunc(d4);
  //       }
  //       // for (var yu = 0; yu < d4.length; yu++) {
  //       //   that.innerFunc(d4[yu]);
  //       // }
  //     }

  //     // that.allData.map.setCameraTarget({ lat: lat, lng: lng });
  //     // if (that.allData.mark == undefined) {

  //     //   that.createMarker(that.startPos, that.getIconUrl());

  //     // } else {
  //     //   // that.allData.mark.setIcon(that.getIconUrl());
  //     //   that.setPositionAnimate(that.dataArrayCoords, that.target);
  //     //   // that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
  //     //   // that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
  //     // }
  //   } else {
  //     var d4: any = [];
  //     that.innerFunc(d4);
  //     // that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
  //   }
  // }

  innerFunc(d4) {
    let that = this;
    var i = 0, howManyTimes = d4.length;
    function f() {

      if (d4[i] == undefined) {
        return;
      }
      // if (data.last_location) {
      // that.latLngArray.push(data.last_location);
      // }
      that.latLngArray.push({
        'lat': d4[i][0],
        'long': d4[i][1]
      });
      // that.otherValues(data);
      if (that.devices.indexOf(d4[i][4].imei) === -1) {
        that.devices.push(d4[i][4].imei);
        const indice = that.devices.indexOf(d4[i][4].imei);
        that.createMarker(d4[i], indice, that.getIconUrl());
      } else {
        const indice = that.devices.indexOf(d4[i][4].imei);
        // that.markersArray[indice].setIcon(that.getIconUrl());
        that.setPositionAnimate(indice);
      }

      if (i === howManyTimes) {
        that.playing = false;
      }

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
    // (function (data) {
    //   if (data == undefined) {
    //     return;
    //   }
    //   // if (data.last_location) {
    //   // that.latLngArray.push(data.last_location);
    //   // }
    //   that.latLngArray.push({
    //     'lat': data[0],
    //     'long': data[1]
    //   });
    //   // that.otherValues(data);
    //   if (that.devices.indexOf(data[4].imei) === -1) {
    //     that.devices.push(data[4].imei);
    //     const indice = that.devices.indexOf(data[4].imei);
    //     that.createMarker(data, indice, that.getIconUrl());
    //   } else {
    //     const indice = that.devices.indexOf(data[4].imei);
    //     // that.markersArray[indice].setIcon(that.getIconUrl());
    //     that.setPositionAnimate(indice);
    //   }
    // })(d4)
  }

  ////////// Anjali's Code starts //////////
  createMarker(location, indice, iconUrl) {
    // debugger
    let that = this;
    let latlng = new LatLng(location[0], location[1]);
    let markerOptions = {
      title: 'Ionic',
      animation: 'BOUNCE',
      position: latlng,
      icon: iconUrl
    };
    that.allData.map.addMarker(markerOptions).then((marker) => {
      that.markersArray[indice] = marker;
      console.log('merkers array list: ', that.markersArray);
    });
  }
  setPositionAnimate(indice) {
    let that = this;
    let nyc, london;
    debugger
    if (that.latLngArray.length >= 2) {
      if (that.fraction >= 1) {
        console.log("when fraction 1: ", that.latLngArray.length)

        clearInterval(that.intevalId);

        if (that.latLngArray.length === 2) {
          nyc = { "lat": Number(that.latLngArray[0].lat), "lng": Number(that.latLngArray[0].long) };
          london = { "lat": Number(that.latLngArray[1].lat), "lng": Number(that.latLngArray[1].long) };
          // this.speed = location.last_location.last_speed * 3.6;
          // let nyc = { "lat": Number(that.latLngArray[0].lat), "lng": Number(that.latLngArray[0].long) };
          // let london = { "lat": Number(that.latLngArray[1].lat), "lng": Number(that.latLngArray[1].long) };
          // let bearing = Spherical.computeHeading(nyc, london)
          // let points = [nyc, london];
          that.latLngArray.shift();
          // this.allData.map.addPolyline({
          //   "points": points,
          //   "geodesic": true,
          //   "width": 3
          // });
          that.fraction = 0;
          // let GOOGLE: ILatLng = london;
          // this.allData.map.animateCamera({
          //   target: GOOGLE,
          //   bearing: bearing,
          //   tilt: 60,
          //   duration: 2500,
          //   zoom: that.zoomLevel
          // });
        } else if (that.latLngArray.length > 2) {
          // var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          // var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          // var bearing = Spherical.computeHeading(nyc, london);
          // var points = [nyc, london];
          that.latLngArray.shift();
          console.log("After shift from 1: ", that.latLngArray.length);
          // this.allData.map.addPolyline({
          //   "points": points,
          //   "geodesic": true,
          //   "width": 3
          // });
          that.fraction = 0;
          // let GOOGLE: ILatLng = london;
          // this.allData.map.animateCamera({
          //   target: GOOGLE,
          //   bearing: bearing,
          //   tilt: 60,
          //   duration: 2500,
          //   zoom: that.zoomLevel
          // });
        }

      } else if (that.fraction === 0) {
        console.log("when fraction 0: ", that.latLngArray.length)
        clearInterval(that.intevalId);
        if (that.latLngArray.length === 2) {
          // var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          // var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          // var bearing = Spherical.computeHeading(nyc, london);
          // var points = [nyc, london];
          that.latLngArray.shift();
          // this.allData.map.addPolyline({
          //   "points": points,
          //   "geodesic": true,
          //   "width": 3
          // });
          // let GOOGLE: ILatLng = london;
          // this.allData.map.animateCamera({
          //   target: GOOGLE,
          //   bearing: bearing,
          //   tilt: 60,
          //   duration: 2500,
          //   zoom: that.zoomLevel
          // });
          console.log("After shift from 0: ", that.latLngArray.length);
        } else if (that.latLngArray.length > 2) {
          // var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          // var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          // var bearing = Spherical.computeHeading(nyc, london);
          // var points = [nyc, london];
          that.latLngArray.shift();
          // this.allData.map.addPolyline({
          //   "points": points,
          //   "geodesic": true,
          //   "width": 3
          // });
          // let GOOGLE: ILatLng = london;
          // this.allData.map.animateCamera({
          //   target: GOOGLE,
          //   bearing: bearing,
          //   tilt: 60,
          //   duration: 2500,
          //   zoom: that.zoomLevel
          // });
        }
      } else if (that.fraction > 0 && that.fraction < 1) {
        clearInterval(that.intevalId);
        if (that.latLngArray.length === 2) {
          // var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          // var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          // var bearing = Spherical.computeHeading(nyc, london);
          // var points = [nyc, london];
          that.latLngArray.shift();
          // this.allData.map.addPolyline({
          //   "points": points,
          //   "geodesic": true,
          //   "width": 3
          // });
          // let GOOGLE: ILatLng = london;
          // this.allData.map.animateCamera({
          //   target: GOOGLE,
          //   bearing: bearing,
          //   tilt: 60,
          //   duration: 2500,
          //   zoom: that.zoomLevel
          // });
          console.log("After shift from 0: ", that.latLngArray.length);
        } else if (that.latLngArray.length > 2) {
          // var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          // var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
          london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
          // var bearing = Spherical.computeHeading(nyc, london);
          // var points = [nyc, london];
          that.latLngArray.shift();
          // this.allData.map.addPolyline({
          //   "points": points,
          //   "geodesic": true,
          //   "width": 3
          // });
          // let GOOGLE: ILatLng = london;
          // this.allData.map.animateCamera({
          //   target: GOOGLE,
          //   bearing: bearing,
          //   tilt: 60,
          //   duration: 2500,
          //   zoom: that.zoomLevel
          // });
        }
      }
      if (that.fraction <= 0 || (that.fraction > 0 && that.fraction < 1)) {
        that.intevalId = setInterval(function () {
          if (that.fraction <= 1) {
            that.fraction += 0.01 * that.direction;
          }
          // debugger
          if (that.markersArray.length > 0) {
            that.markersArray[indice].setPosition(Spherical.interpolate(nyc, london, that.fraction)); let bearing = Spherical.computeHeading(nyc, london)

            let GOOGLE: ILatLng = london;
            that.allData.map.animateCamera({
              target: GOOGLE,
              bearing: bearing,
              tilt: 60,
              duration: 2500,
              zoom: that.zoomLevel
            });
          }
        }, 100);
      }
      // else {
      //   that.latLngArray.shift();
      // }
    } else {
      return;
    }
  }
  /////////// Anjali's code end ///////////

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  liveTrack(map, mark, coords, target, startPos, speed, delay) {
    let that = this;
    that.events.subscribe("SpeedValue:Updated", (sdata) => {
      speed = sdata;
    })
    var target = target;
    clearTimeout(that.ongoingGoToPoint[coords[target][4].imei]);
    clearTimeout(that.ongoingMoveMarker[coords[target][4].imei]);
    console.log("check coord imei: ", coords[target][4].imei);
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(new LatLng(lat, lng));
          that.recenterMeLat = lat;
          that.recenterMeLng = lng;
          that.getAddress(lat, lng);
          // map.setCameraTarget(new LatLng(lat, lng))
          that.ongoingMoveMarker[coords[target][4].imei] = setTimeout(_moveMarker, delay);
        } else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(dest);
          that.recenterMeLat = dest.lat;
          that.recenterMeLng = dest.lng;
          that.getAddress(dest.lat, dest.lng);
          // map.setCameraTarget(dest);
          target++;
          that.ongoingGoToPoint[coords[target][4].imei] = setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        that.speedMarker = coords[target][3].speed;
        that.updatetimedate = coords[target][2].time;
        that.cumu_distance = coords[target][5].cumu_dist;

        if (that.playing) {
          _moveMarker();
          target = target;
          localStorage.setItem("markerTarget", target);

        } else { }
        // km_h = km_h;
      }
    }
    var a = 0;
    _gotoPoint();
  }

  getAddress(lat, lng) {
    let that = this;
    var coordinates = {
      lat: lat,
      long: lng
    };
    if (!coordinates) {
      that.addressLine = 'N/A';
      return;
    }
    this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
        that.addressLine = str;
      })
  }

  zoomin() {
    let that = this;
    that.allData.map.animateCameraZoomIn()
    // that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  inter(fastforwad) {
    // debugger
    let that = this;
    console.log("fastforwad=> " + fastforwad);
    if (fastforwad == 'fast') {
      that.speed = 2 * that.speed;
      console.log("speed fast=> " + that.speed)
    }
    else if (fastforwad == 'slow') {
      if (that.speed > 50) {
        that.speed = that.speed / 2;
        console.log("speed slow=> " + that.speed)
      }
      else {
        console.log("speed normal=> " + that.speed)
      }
    }
    else {
      that.speed = 200;
    }
    that.events.publish("SpeedValue:Updated", that.speed)
  }

  btnClicked(timeStart, timeEnd) {
    if (localStorage.getItem("MainHistory") != null) {
      if (this.selectedVehicle == undefined) {
        let alert = this.alertCtrl.create({
          message: "Please select the vehicle first!!",
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.maphistory(timeStart, timeEnd);
      }
    } else {
      this.maphistory(timeStart, timeEnd);
    }
  }

  maphistory(timeStart, timeEnd) {

    var from1 = new Date(timeStart);
    this.fromtime = from1.toISOString();
    var to1 = new Date(timeEnd);
    this.totime = to1.toISOString();

    if (this.totime >= this.fromtime) {

    } else {
      let alert = this.alertCtrl.create({
        title: 'Select Correct Time',
        message: 'To time always greater than From Time',
        buttons: ['ok']
      });
      alert.present();
      return false;
    }

    this.apiCall.startLoading().present();
    this.apiCall.getDistanceSpeedCall(this.trackerId, this.fromtime, this.totime)
      .subscribe(data3 => {
        this.data2 = data3;
        this.latlongObjArr = data3;

        if (this.data2["Average Speed"] == 'NaN') {
          this.data2.AverageSpeed = 0;
        } else {
          this.data2.AverageSpeed = this.data2["Average Speed"];
        }

        this.data2.IdleTime = this.data2["Idle Time"];
        this.hideplayback = true;
        // this.customTxt = "<html> <head><style> </style> </head><body>Total Distance - " + this.total_dis + " Km<br>Average Speed - " + this.avg_speed + " Km/hr</body> </html> "

        //////////////////////////////////
        this.callgpsFunc(this.fromtime, this.totime);

        // this.locations = [];
        // this.stoppages(timeStart, timeEnd);
        ////////////////////////////////
      },
        error => {
          this.apiCall.stopLoading();
          console.log("error in getdistancespeed =>  ", error)
          var body = error._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });

  }

  stoppages() {
    this.locations = [];
    let that = this;
    that.apiCall.stoppage_detail(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.DeviceId)
      .subscribe(res => {
        console.log('stoppage data', res)
        var arr = [];
        for (var i = 0; i < res.length; i++) {

          this.arrivalTime = new Date(res[i].arrival_time).toLocaleString();
          this.departureTime = new Date(res[i].departure_time).toLocaleString();

          var fd = new Date(this.arrivalTime).getTime();
          var td = new Date(this.departureTime).getTime();
          var time_difference = td - fd;
          var total_min = time_difference / 60000;
          var hours = total_min / 60
          var rhours = Math.floor(hours);
          var minutes = (hours - rhours) * 60;
          var rminutes = Math.round(minutes);
          var Durations = rhours + 'Hours' + ':' + rminutes + 'Min';

          arr.push({
            lat: res[i].lat,
            lng: res[i].long,
            arrival_time: res[i].arrival_time,
            departure_time: res[i].departure_time,
            device: res[i].device,
            address: res[i].address,
            user: res[i].user,
            duration: Durations
          });

          that.locations.push(arr);
          if (that.locations[0] != undefined) {              // check if there is stoppages or not
            for (var k = 0; k < that.locations[0].length; k++) {
              that.setStoppages(that.locations[0][k]);
            }
          }

        }
        console.log('stoppage data locations', that.locations)
        // this.callgpsFunc(this.fromtime, this.totime);
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });
  }

  callgpsFunc(fromtime, totime) {
    let that = this;
    that.apiCall.gpsCall(this.trackerId, fromtime, totime)
      .subscribe(data3 => {
        that.apiCall.stopLoading();
        if (data3.length > 0) {
          if (data3.length > 1) {   // to draw polyline at least need two points
            that.gps(data3.reverse());
          } else {
            let alert = that.alertCtrl.create({
              message: 'No Data found for selected vehicle..',
              buttons: [{
                text: 'OK',
                handler: () => {
                  // that.datetimeStart = moment({ hours: 0 }).format();
                  // console.log('start date', this.datetimeStart)
                  // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                  // console.log('stop date', this.datetimeEnd);

                  // that.selectedVehicle = undefined;
                  that.hideplayback = false;
                }
              }]
            });
            alert.present();
          }
        } else {
          let alert = that.alertCtrl.create({
            message: 'No Data found for selected vehicle..',
            buttons: [{
              text: 'OK',
              handler: () => {
                // that.datetimeStart = moment({ hours: 0 }).format();
                // console.log('start date', this.datetimeStart)
                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                // console.log('stop date', this.datetimeEnd);
                // that.selectedVehicle = undefined;
                that.hideplayback = false;
              }
            }]
          });
          alert.present();
        }

      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = that.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  gps(data3) {

    let that = this;
    that.latlongObjArr = data3;
    that.dataArrayCoords = [];
    for (var i = 0; i < data3.length; i++) {
      if (data3[i].lat && data3[i].lng) {
        var arr = [];
        var cumulativeDistance = 0;
        var startdatetime = new Date(data3[i].insertionTime);
        arr.push(data3[i].lat);
        arr.push(data3[i].lng);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": data3[i].speed });
        arr.push({ "imei": data3[i].imei })
        // debugger
        if (data3[i].isPastData != true) {
          if (i === 0) {
            cumulativeDistance += 0;
          } else {
            cumulativeDistance += data3[i].distanceFromPrevious ? parseFloat(data3[i].distanceFromPrevious) : 0;
          }
          data3[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
          arr.push({ "cumu_dist": data3[i]['cummulative_distance'] });
        } else {
          data3[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
          arr.push({ "cumu_dist": data3[i]['cummulative_distance'] });
        }
        // console.log("cummulative_distance: ", data3[i]['cummulative_distance'])
        that.dataArrayCoords.push(arr);
      }
    }

    that.mapData = [];
    that.mapData = data3.map(function (d) {
      return { lat: d.lat, lng: d.lng };
    })
    that.mapData.reverse();

    if (that.allData.map != undefined) {
      that.allData.map.remove();
    }

    let bounds = new LatLngBounds(that.mapData);

    let mapOptions = {
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: that.mapKey
    }

    that.allData.map = GoogleMaps.create('map_canvas', mapOptions);
    // that.allData.map.moveCamera({
    //   target: bounds
    // })
    that.allData.map.animateCamera({
      target: bounds
    })
    this.allData.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
      (data) => {
        console.log('Click MAP');

        that.drawerHidden1 = true;
      }
    );
    // if (that.locations[0] != undefined) {              // check if there is stoppages or not
    //   for (var k = 0; k < that.locations[0].length; k++) {
    //     that.setStoppages(that.locations[0][k]);
    //   }
    // }

    that.allData.map.addMarker({
      title: 'D',
      position: that.mapData[0],
      icon: 'red',
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      marker.showInfoWindow();

      that.allData.map.addMarker({
        title: 'S',
        position: that.mapData[that.mapData.length - 1],
        icon: 'green',
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
    });

    that.allData.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    })
  }

  setStoppages(pdata) {
    let that = this;
    ///////////////////////////////
    // let htmlInfoWindow = new HtmlInfoWindow();
    // let frame: HTMLElement = document.createElement('div');
    // frame.innerHTML = [
    //   '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
    //   '<p style="font-size: 7px;">Arrival Time:- ' + moment(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
    //   '<p style="font-size: 7px;">Departure Time:- ' + moment(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
    // ].join("");

    // htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
    ///////////////////////////////////////////////////

    if (pdata != undefined)
      (function (data) {
        console.log("inside for data=> ", data)

        var centerMarker = data;
        let location = new LatLng(centerMarker.lat, centerMarker.lng);
        var markicon;
        if (that.plt.is('ios')) {
          markicon = 'www/assets/imgs/park.png';
        } else if (that.plt.is('android')) {
          markicon = './assets/imgs/park.png';
        }
        let markerOptions = {
          position: location,
          icon: markicon
        };
        that.allData.map.addMarker(markerOptions)
          .then((marker: Marker) => {
            // console.log('centerMarker.ID' + centerMarker.ID)
            marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(e => {
                that.showActionSheet = true;
                // that.drawerHidden1 = false;
                that.drawerState = DrawerState.Docked;
                Geocoder.geocode({
                  "position": {
                    lat: e[0].lat,
                    lng: e[0].lng
                  }
                }).then((results: GeocoderResult[]) => {
                  if (results.length == 0) {
                    return null;
                  }
                  that.addressof = results[0].extra.lines[0];
                });

                setTimeout(function () {

                  that.address = that.addressof;
                  console.log("pickup location new ", that.address);
                  that.arrTime = moment(new Date(data.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                  that.depTime = moment(new Date(data.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");

                  var fd = new Date(data.arrival_time).getTime();
                  var td = new Date(data.departure_time).getTime();
                  var time_difference = td - fd;
                  var total_min = time_difference / 60000;
                  var hours = total_min / 60
                  var rhours = Math.floor(hours);
                  var minutes = (hours - rhours) * 60;
                  var rminutes = Math.round(minutes);
                  that.durations = rhours + 'hours' + ':' + rminutes + 'mins'
                }, 500);

              });
          });

      })(pdata)
  }

  onIdle() {
    this.presentModal();

  }
  presentModal() {
    const modal = this.modalCtrl.create(ModalPage);
    modal.present();

    modal.onDidDismiss((data) => {
      console.log("onDidDismiss", data);
      this.getIdlePoints(data);
    })
  }

  getIdlePoints(min) {
    this.idleLocations = [];
    var urlbase = this.apiCall.mainUrl + 'stoppage/trip_idle?uId=' + this.islogin._id + '&from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&device=' + this.DeviceId + '&min_time=' + min;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(urlbase)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("idle data=> " + data);
        if (data.length > 0) {
          for (var y = 0; y <= data.length; y++) {
            this.idleLocations.push(data[y]);
          }

          if (this.idleLocations.length > 0) {              // check if there is stoppages or not
            for (var k = 0; k < this.idleLocations.length; k++) {
              this.setIdlePoints(this.idleLocations[k]);
            }
          }
        }
      })
  }

  setIdlePoints(pdata) {
    let that = this;
    if (pdata != undefined)
      (function (data) {
        console.log("inside for data=> ", data)

        var centerMarker = data;
        let location = new LatLng(centerMarker.idle_location.lat, centerMarker.idle_location.long);
        var markicon;
        if (that.plt.is('ios')) {
          markicon = 'www/assets/imgs/idle.png';
        } else if (that.plt.is('android')) {
          markicon = './assets/imgs/idle.png';
        }
        let markerOptions = {
          position: location,
          icon: markicon
        };
        that.allData.map.addMarker(markerOptions)
          .then((marker: Marker) => {
            // console.log('centerMarker.ID' + centerMarker.ID)
            marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(e => { });
          });

      })(pdata)
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }
  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }
}
